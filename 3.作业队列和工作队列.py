from multiprocessing import Process
from multiprocessing.queues import Queue
import time
import random

WORKER = []


# data_piple = Queue(10)

def ff(a):

    time.sleep(random.randint(1, 5))
    print(a)


def listener(data: list):
    while data:
        if len(WORKER) > 5:
            print('满')
        else:
            t = Process(target=ff, args=(data.pop(),))
            WORKER.append(t)
            t.start()
            # t.join()


def worker():
    while WORKER:
        for num, i in enumerate(WORKER):
            if not i.is_alive():
                WORKER.pop(num)


if __name__ == '__main__':
    t1 = Process(target=listener, args=([1, 2, 3, 4, 5],))
    t2 = Process(target=worker)
    t1.start()
    t2.start()
    t1.join()
    t2.join()
