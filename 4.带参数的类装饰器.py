from functools import wraps
from datetime import datetime
import time


class MyClass:
    __slots__ = ['_name']

    def __init__(self, *, name: str):
        self._name = name

    def __call__(self, func, /):
        @wraps(func)
        def temp(*args, **kwargs):
            t1 = datetime.now()
            func(*args, **kwargs)
            print(f'该段代码运行时间：{datetime.now() - t1}s')

        return temp


@MyClass(name='200')
def a():
    for i in range(3):
        time.sleep(i)


if __name__ == '__main__':
    a()
