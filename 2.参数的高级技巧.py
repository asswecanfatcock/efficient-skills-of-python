def use_func(a, b, /, *, c):
    """
    /左边只能使用位置参数，*右边只能使用指定参数

    """
    print(a, b, c)


if __name__ == '__main__':
    use_func(1, 2, c=3)
    use_func(a=1, b=2, c=3)
