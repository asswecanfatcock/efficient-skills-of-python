from pprint import pprint


class People:
    money: int
    name: str

    def __init__(self, money: int, name: str):
        self.money = money
        self.name = name

    def __repr__(self):
        return f"People({self.name}, {self.money})"

def sort_test(people_room: list) -> None:
    """
    利用元组的比较特性：依次比较对应位置大小，直到可以确定大小为止

    :param people_room:
    :return:
    """
    people_room.sort(key=lambda x: (x.money, x.name))


if __name__ == '__main__':
    a = [People(1, "aaa"),
         People(2, "bbb"),
         People(0, "ccc"),
         People(5, "ddd")]
    sort_test(a)
    pprint(a)
